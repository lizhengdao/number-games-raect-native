import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import Header from "./Components/Header";
import StartGameScreen from "./Screens/StartGame";
import GameScreen from "./Screens/GameScreen";
import GameOver from "./Screens/GameOver";
export default function App() {
  const [userNumber, SetNumber] = useState();
  const [rounds, SetRounds] = useState(0);

  const gameOverHandler = (numRounds) => {
    SetRounds(numRounds);
   
  };
const restartGame= ()=>{
  SetRounds(0)
  SetNumber(null)
}
  const StartGameHandler = (selectedNumber) => {
    SetNumber(selectedNumber);
    SetRounds(0);
  };
  let content = <StartGameScreen onStartGame={StartGameHandler} />;
  if (userNumber && rounds <= 0) {
    content = (
      <GameScreen userChoice={userNumber} onGameOver={gameOverHandler} />
    );
  } else if (rounds > 0) {
    content = <GameOver roundsNumber={rounds} userNumber={userNumber} restart={restartGame} />;
  }
  return (
    <View style={styles.screen}>
      <Header Title="Guess How Many" />
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
});
