import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Colors from '../Constants/Color'
const Header = (props) => {
  return (
    <View style={styles.Header}>
      <Text style={styles.headerTitle}>{props.Title}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  Header: {
    width: "100%",
    height: 90,
    paddingTop: 36,
    backgroundColor: Colors.primary,
    alignItems: "center",
    justifyContent: "center",
  },
  headerTitle: {
    color: "white",
    fontWeight: "bold",
    fontSize: 25,
  },
});
export default Header;
