import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  Text,
  Keyboard,
  View,
  TouchableWithoutFeedback,
  Button,
  Alert,
} from "react-native";
import Inputbox from "../Components/Input";
import Card from "../Components/Card";
import NumberCon from "../Components/NumberContainer";
import Colors from "../Constants/Color";

const genrateRandomNumber = (min, max, exclude) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  const rndNumber = Math.floor(Math.random() * (max - min)) + min;
  if (rndNumber === exclude) {
    return genrateRandomNumber(min, max, exclude);
  } else {
    return rndNumber;
  }
};

const GameScreen = (props) => {
  const [currentGuess, SetGuess] = useState(
    genrateRandomNumber(1, 100, props.userChoice)
  );
  const {userChoice,onGameOver} = props;
  const [rounds, SetRounds] = useState(0)
  const currentLow = useRef(1);
  const currentHigh = useRef(1);
useEffect(()=>{
    if(currentGuess === userChoice)
    {
       onGameOver(rounds);
    }
},[currentGuess,userChoice,onGameOver]);
  const nextGuessHandler = (direction) => {
    if (
      (direction === "lower" && currentGuess < props.userChoice) ||
      (direction === "greater" && currentGuess > props.userChoice)
    ) {
      Alert.alert("Be Truthful", "You know THat this is Wrong", [
        { text: "Sorry", style: "cancel" },
      ]);
      return;
    }
    if (direction === "lower") {
      currentHigh.current = currentGuess;
    } else {
      currentLow.current = currentGuess;
    }
    const nextNumber = genrateRandomNumber(
      currentLow.current,
      currentHigh.current,
      currentGuess
    );
    SetGuess(nextNumber);
    SetRounds(curRounds => curRounds + 1)
  };
  return (
    <View style={styles.screen}>
      <Text>Opponent Guess</Text>
      <NumberCon>{currentGuess}</NumberCon>
      <Card style={styles.buttonContainer}>
        <Button color={Colors.accent} title="Lower" onPress={() => nextGuessHandler("lower")} />
        <Button color={Colors.primary} title="Higher" onPress={() => nextGuessHandler("greater")} />
      </Card>
    </View>
  );
};
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20,
    width: 300,
    maxWidth: "80%",
  },
});

export default GameScreen;
