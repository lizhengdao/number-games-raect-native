import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  Text,
  Keyboard,
  View,
  TouchableWithoutFeedback,
  Button,
  Alert,
} from "react-native";
import Inputbox from "../Components/Input";
import Card from "../Components/Card";
import NumberCon from "../Components/NumberContainer";
import Colors from "../Constants/Color";
const GameOver = (props) => {
  return (
    <View style={styles.screen}>
      <View style={styles.title}>
        <Text style={styles.title}>Game Over</Text>
      </View>

      <View style={styles.subtitle}>
        <Text style={styles.subtitle}>Number of Rounds: {props.roundsNumber}</Text>
      </View>
      <View style={styles.subtitle}>
        <Text style={styles.subtitle}>Number Was: {props.userNumber}</Text>
      </View>

      <View style={styles.button}>
        <Button  color={Colors.accent} title="New Game" onPress={props.restart} />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title:{
      fontSize: 30,
      color: Colors.accent,
     
  },
   subtitle:{
    fontSize: 20,
    color: Colors.primary,
    
},
    button:{
        marginVertical:10
    }
});
export default GameOver;
